import { ChallengeSoftruckPage } from './app.po';

describe('challenge-softruck App', () => {
  let page: ChallengeSoftruckPage;

  beforeEach(() => {
    page = new ChallengeSoftruckPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
